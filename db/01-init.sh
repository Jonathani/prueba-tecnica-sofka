#!/bin/bash
set -e
export PGPASSWORD=$POSTGRES_PASSWORD;
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
 CREATE USER $APP_DB_USER_LOCAL WITH PASSWORD '$APP_DB_PASS_LOCAL';
 ALTER ROLE $APP_DB_USER_LOCAL with SUPERUSER;
 CREATE DATABASE $APP_DB_NAME_LOCAL;
 GRANT ALL PRIVILEGES ON DATABASE $APP_DB_NAME_LOCAL TO $APP_DB_USER_LOCAL;
 GRANT ALL ON DATABASE $APP_DB_NAME_LOCAL TO $APP_DB_USER_LOCAL;
 ALTER DATABASE $APP_DB_NAME_LOCAL OWNER TO $APP_DB_USER_LOCAL;
 GRANT USAGE, CREATE ON SCHEMA PUBLIC TO $APP_DB_USER_LOCAL;
 GRANT ALL ON SCHEMA public TO $APP_DB_USER_LOCAL;
 GRANT USAGE ON schema public TO $APP_DB_USER_LOCAL;
 GRANT SELECT ON ALL TABLES IN SCHEMA public TO $APP_DB_USER_LOCAL;
 GRANT CREATE ON SCHEMA public TO $APP_DB_USER_LOCAL;
 \connect $APP_DB_NAME_LOCAL $APP_DB_USER_LOCAL
 BEGIN;

    --Persona
    CREATE TABLE public.persona
    (
    persona_id numeric not null
    , nombre text
    , genero text
    , edad numeric(38, 2)
    , identificacion text
    , direccion text
    , telefono text
    , CONSTRAINT persona_pkey PRIMARY KEY (persona_id)
    );

    CREATE SEQUENCE public.seq_persona
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;
            
    --Cliente
    CREATE TABLE public.cliente
    (
    cliente_id numeric not null
    , contrasenia text
    , estado boolean
    , persona_id numeric
    , CONSTRAINT cliente_pkey PRIMARY KEY (cliente_id)
    );
    ALTER TABLE public.cliente ADD CONSTRAINT fk_persona FOREIGN KEY (persona_id) REFERENCES public.persona(persona_id);

    CREATE SEQUENCE public.seq_cliente
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;
        
    --Cuenta
    CREATE TABLE public.cuenta
    (
    cuenta_id numeric not null
    , numero_cuenta text
    , tipo_cuenta text
    , saldo_inicial numeric (38, 2)
    , estado boolean
    , cliente_id numeric
    , CONSTRAINT cuenta_pkey PRIMARY KEY (cuenta_id)
    );
    ALTER TABLE public.cuenta ADD CONSTRAINT fk_cliente FOREIGN KEY (cliente_id) REFERENCES public.cliente(cliente_id);


    CREATE SEQUENCE public.seq_cuenta
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;
        
    --Movimientos
    CREATE TABLE public.movimientos
    (
    movimientos_id numeric not null
    , fecha timestamp null
    , tipo_movimiento text
    , valor numeric (38, 2)
    , saldo numeric (38, 2)
    , cuenta_id numeric
    , CONSTRAINT movimientos_pkey PRIMARY KEY (movimientos_id)
    );
    ALTER TABLE public.movimientos ADD CONSTRAINT fk_cuenta FOREIGN KEY (cuenta_id) REFERENCES public.cuenta(cuenta_id);

    CREATE SEQUENCE public.seq_movimientos
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;

COMMIT;

EOSQL
