package com.ec.pruebaSofka.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ec.pruebaSofka.entity.Movimiento;
import com.ec.pruebaSofka.sql.ConsultasSQL;

@Repository
public interface MovimientosDao extends CrudRepository<Movimiento, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT movimiento FROM Movimiento movimiento"
			+ " JOIN movimiento.cuenta cuenta"
			+ " WHERE cuenta.numeroCuenta = :numeroCuenta"
			+ " ORDER BY movimiento.fecha DESC")
	List<Movimiento> findMovimientoByNumeroCta(String numeroCuenta, Pageable pageable);
	
	@Query(value = ConsultasSQL.CONSULTA_ESTADO_CUENTA, nativeQuery = true)
	List<Object[]> consultarEstadoCuenta(String identificacion, Date fechaInicio, Date fechaFin);
}
