package com.ec.pruebaSofka.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ec.pruebaSofka.entity.Persona;

@Repository
public interface PersonaDao extends CrudRepository<Persona, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT persona FROM Persona persona WHERE persona.identificacion = :identificacion")
	Persona findPersonaById(String identificacion);

}
