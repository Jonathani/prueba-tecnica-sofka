package com.ec.pruebaSofka.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ec.pruebaSofka.entity.Cliente;

@Repository
public interface ClienteDao extends CrudRepository<Cliente, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT cliente FROM Cliente cliente WHERE cliente.clienteId = :clienteId")
	Cliente findClienteById(Long clienteId);

}
