package com.ec.pruebaSofka.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ec.pruebaSofka.entity.Cuenta;

@Repository
public interface CuentaDao extends CrudRepository<Cuenta, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT cuenta FROM Cuenta cuenta WHERE cuenta.numeroCuenta = :numeroCuenta")
	Cuenta findCuentaByNumeroCta(String numeroCuenta);

}
