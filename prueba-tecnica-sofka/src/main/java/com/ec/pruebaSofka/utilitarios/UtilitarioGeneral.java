package com.ec.pruebaSofka.utilitarios;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;

import com.ec.pruebaSofka.entity.Movimiento;

public class UtilitarioGeneral {

	public static final String BANCO_DE_PALABRAS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!$";

	public static String generarClaveAleatoria(Long longitud) {

		String cadena = "";

		for (int x = 0; x < longitud; x++) {
			int indiceAleatorio = numeroAleatorioEnRango(0, UtilitarioGeneral.BANCO_DE_PALABRAS.length() - 1);
			char caracterAleatorio = UtilitarioGeneral.BANCO_DE_PALABRAS.charAt(indiceAleatorio);
			cadena += caracterAleatorio;
		}
		return cadena;
	}

	public static int numeroAleatorioEnRango(int minimo, int maximo) {
		return ThreadLocalRandom.current().nextInt(minimo, maximo + 1);
	}

	public static BigDecimal obtenerEdadPersona(String fechaNacimiento) {

		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
		LocalDate ahora = LocalDate.now();

		Period periodo = Period.between(fechaNac, ahora);

		BigDecimal edadEnAnios = new BigDecimal(periodo.getYears());

		return edadEnAnios;
	}

	public static boolean isNumeric(String cadena) {

		boolean resultado;

		try {
			Integer.parseInt(cadena);
			resultado = true;
		} catch (NumberFormatException excepcion) {
			resultado = false;
		}

		return resultado;
	}
	
	public static BigDecimal saldoDisponible(float valor, Movimiento movimiento) {		
		
		BigDecimal saldo = movimiento.getSaldo();
		BigDecimal valorMovimiento = new BigDecimal(valor);		
		saldo = saldo.add(valorMovimiento);	
		
		return saldo;
	}

}
