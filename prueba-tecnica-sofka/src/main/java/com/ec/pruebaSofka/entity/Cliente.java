package com.ec.pruebaSofka.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the cliente database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="cliente", schema = "public")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_cliente", sequenceName = "public.seq_cliente", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "seq_cliente")
	@Column(name="cliente_id")
	private long clienteId;

	private String contrasenia;

	private Boolean estado;

	//bi-directional many-to-one association to Persona
	@OneToOne
	@JoinColumn(name="persona_id")
	private Persona persona;
	
	//bi-directional many-to-one association to Cuenta
	@OneToMany(mappedBy="cliente")
	private List<Cuenta> cuentas;
	
}