package com.ec.pruebaSofka.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the persona database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="persona", schema = "public")
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_persona", sequenceName = "public.seq_persona", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "seq_persona")
	@Column(name="persona_id")
	private long personaId;

	private String direccion;

	private BigDecimal edad;

	private String genero;

	private String identificacion;

	private String nombre;

	private String telefono;

	//bi-directional many-to-one association to Cliente
	@OneToOne(mappedBy="persona")
	private Cliente cliente;

}