package com.ec.pruebaSofka.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the movimientos database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="movimientos", schema = "public")
@NamedQuery(name="Movimiento.findAll", query="SELECT m FROM Movimiento m")
public class Movimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_movimientos", sequenceName = "public.seq_movimientos", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "seq_movimientos")
	@Column(name="movimientos_id")
	private long movimientosId;

	private Timestamp fecha;

	private BigDecimal saldo;

	@Column(name="tipo_movimiento")
	private String tipoMovimiento;

	private BigDecimal valor;
	
	//bi-directional many-to-one association to Cuenta
	@ManyToOne
	@JoinColumn(name="cuenta_id")
	private Cuenta cuenta;

}