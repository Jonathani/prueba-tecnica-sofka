package com.ec.pruebaSofka.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ec.pruebaSofka.dto.ClienteEditRequestDto;
import com.ec.pruebaSofka.dto.ClienteRequestDto;
import com.ec.pruebaSofka.dto.EstadoCuentaRequestDto;
import com.ec.pruebaSofka.dto.EstadoCuentaResponseDto;
import com.ec.pruebaSofka.dto.ListaEstadoCuentaResponseDto;
import com.ec.pruebaSofka.dto.MensajeResponseDto;
import com.ec.pruebaSofka.dto.MovimientoRequestDto;
import com.ec.pruebaSofka.service.ClienteService;
import com.ec.pruebaSofka.service.MovimientosService;
import com.ec.pruebaSofka.utilitarios.UtilitarioMensajes;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class ApiRestController {

	@Autowired
	ClienteService clienteService;
	
	@Autowired
	MovimientosService movimientosService;

	public static final String MENSAJE = "mensaje";

	@PostMapping(value = "crearCliente", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> crearCliente(@RequestBody ClienteRequestDto clienteRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		MensajeResponseDto mensajeResponseDto;
		try {
			clienteService.pushCliente(clienteRequestDto);
			mensajeResponseDto = new MensajeResponseDto(true, UtilitarioMensajes.GUARDADO_EXITO);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<MensajeResponseDto>(mensajeResponseDto, HttpStatus.OK);
	}

	@PutMapping(value = "editarCliente/{idCliente}", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> editarCliente(@PathVariable("idCliente") Long idCliente,
			@RequestBody ClienteEditRequestDto clienteEditRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		MensajeResponseDto mensajeResponseDto;
		try {
			clienteService.putCliente(idCliente, clienteEditRequestDto);
			mensajeResponseDto = new MensajeResponseDto(true, UtilitarioMensajes.ACTUALIZADO_EXITO);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<MensajeResponseDto>(mensajeResponseDto, HttpStatus.OK);
	}

	@DeleteMapping(value = "borrarCliente/{idCliente}", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> borrarCliente(@PathVariable("idCliente") Long idCliente) {
		Map<String, Object> responseMap = new HashMap<>();
		MensajeResponseDto mensajeResponseDto;

		try {
			clienteService.deleteCliente(idCliente);
			mensajeResponseDto = new MensajeResponseDto(true, UtilitarioMensajes.BORRADO_EXITO);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<MensajeResponseDto>(mensajeResponseDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "registroMovimientos", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> registroMovimientos(@RequestBody MovimientoRequestDto movimientoRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		MensajeResponseDto mensajeResponseDto;
		try {
			movimientosService.pushMovimientos(movimientoRequestDto);
			mensajeResponseDto = new MensajeResponseDto(true, UtilitarioMensajes.VALORES_MOVIMIENTO_EXITOSO);
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<MensajeResponseDto>(mensajeResponseDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "reportes", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> reportes(@RequestBody EstadoCuentaRequestDto estadoCuentaRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		List<EstadoCuentaResponseDto> reportes = new ArrayList<>();
		ListaEstadoCuentaResponseDto listaEstadoCuentaResponseDto;
		try {			
			reportes = movimientosService.getDetalleMovimientos(estadoCuentaRequestDto);
			listaEstadoCuentaResponseDto = new ListaEstadoCuentaResponseDto(true, "Consulta Correcta", reportes);			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ListaEstadoCuentaResponseDto>(listaEstadoCuentaResponseDto, HttpStatus.OK);
	}

}
