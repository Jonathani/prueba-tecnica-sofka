package com.ec.pruebaSofka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MensajeResponseDto {
	
	private Boolean estado;
	
	private String mensaje;

}
