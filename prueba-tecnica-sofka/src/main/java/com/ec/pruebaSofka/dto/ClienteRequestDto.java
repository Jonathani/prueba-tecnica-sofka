package com.ec.pruebaSofka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClienteRequestDto {
	
	private String identificacion;
	
	private String nombre;
	
	private String genero;
	
	private String fechaNacimiento;
	
	private String direccion;
	
	private String telefono;	

}
