package com.ec.pruebaSofka.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ListaEstadoCuentaResponseDto {
	
	private Boolean estado;
	
	private String mensaje;
	
	private List<EstadoCuentaResponseDto> reportes;

}
