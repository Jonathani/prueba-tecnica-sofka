package com.ec.pruebaSofka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoCuentaRequestDto {
	
	private String identificacion;
	
	private String fechaInicio;
	
	private String fechaFin;

}
