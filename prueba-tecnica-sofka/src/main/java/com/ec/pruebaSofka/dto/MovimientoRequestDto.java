package com.ec.pruebaSofka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovimientoRequestDto {
	
	private String numeroDeCuenta;
	
	private String tipoCuenta;
	
	private String valor;
	
	private String clienteId;

}
