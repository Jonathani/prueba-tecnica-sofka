package com.ec.pruebaSofka.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ec.pruebaSofka.dao.ClienteDao;
import com.ec.pruebaSofka.dao.CuentaDao;
import com.ec.pruebaSofka.dao.MovimientosDao;
import com.ec.pruebaSofka.dto.EstadoCuentaRequestDto;
import com.ec.pruebaSofka.dto.EstadoCuentaResponseDto;
import com.ec.pruebaSofka.dto.MovimientoRequestDto;
import com.ec.pruebaSofka.entity.Cliente;
import com.ec.pruebaSofka.entity.Cuenta;
import com.ec.pruebaSofka.entity.Movimiento;
import com.ec.pruebaSofka.service.MovimientosService;
import com.ec.pruebaSofka.utilitarios.UtilitarioGeneral;
import com.ec.pruebaSofka.utilitarios.UtilitarioMensajes;

@Service
public class MoviemientosServiceImpl implements MovimientosService {

	@Autowired
	CuentaDao cuentaDao;

	@Autowired
	MovimientosDao movimientosDao;

	@Autowired
	ClienteDao clienteDao;

	@Override
	public void pushMovimientos(MovimientoRequestDto movimientoRequestDto) throws Exception {

		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		Cuenta cuentaMovimiento;
		Movimiento movimientoCta;
		Cliente cliente = clienteDao.findClienteById(Long.parseLong(movimientoRequestDto.getClienteId()));

		if (cliente != null) {
			
			if (UtilitarioGeneral.isNumeric(movimientoRequestDto.getValor())) {

				if (cuentaDao.findCuentaByNumeroCta(movimientoRequestDto.getNumeroDeCuenta().trim()) == null) {

					if (Float.parseFloat(movimientoRequestDto.getValor()) > 0) {

						Cuenta cuenta = new Cuenta();
						cuenta.setNumeroCuenta(movimientoRequestDto.getNumeroDeCuenta());
						cuenta.setTipoCuenta(movimientoRequestDto.getTipoCuenta());
						cuenta.setSaldoInicial(new BigDecimal(movimientoRequestDto.getValor()));
						cuenta.setEstado(true);
						cuenta.setCliente(cliente);
						cuentaDao.save(cuenta);

						Movimiento movimiento = new Movimiento();
						movimiento.setFecha(fechaActual);
						String tipoMovimiento = Long.parseLong(movimientoRequestDto.getValor().trim()) >= 0 ? "Deposito" : "Retiro";
						movimiento.setTipoMovimiento(tipoMovimiento);
						movimiento.setValor(new BigDecimal(movimientoRequestDto.getValor()));
						movimiento.setSaldo(new BigDecimal(movimientoRequestDto.getValor()));
						movimiento.setCuenta(cuenta);
						movimientosDao.save(movimiento);

					} else {
						throw new Exception("Estimado Usuario el saldo inicial de apertura del numero de cuenta " + movimientoRequestDto.getNumeroDeCuenta() + " debe ser positivo!");
					}
				} else {
					cuentaMovimiento = cuentaDao.findCuentaByNumeroCta(movimientoRequestDto.getNumeroDeCuenta().trim());
					movimientoCta = movimientosDao.findMovimientoByNumeroCta(movimientoRequestDto.getNumeroDeCuenta().trim(), PageRequest.of(0, 1)).get(0);
					BigDecimal saldoDisponible = UtilitarioGeneral.saldoDisponible(Float.parseFloat(movimientoRequestDto.getValor().trim()), movimientoCta);

					if (saldoDisponible.compareTo(BigDecimal.ZERO) >= 0) {
						Movimiento movimiento = new Movimiento();
						movimiento.setFecha(fechaActual);
						String tipoMovimiento = Long.parseLong(movimientoRequestDto.getValor().trim()) >= 0 ? "Deposito" : "Retiro";
						movimiento.setTipoMovimiento(tipoMovimiento);
						movimiento.setValor(new BigDecimal(movimientoRequestDto.getValor().trim()));
						movimiento.setSaldo(saldoDisponible);
						movimiento.setCuenta(cuentaMovimiento);
						movimientosDao.save(movimiento);
					} else {
						throw new Exception(UtilitarioMensajes.SALDO_NO_DISPONIBLE);
					}
				}

			} else {
				throw new Exception(UtilitarioMensajes.VALORES_MOVIMIENTO_INCORRECTO);
			}
		}else {
			throw new Exception(UtilitarioMensajes.USUARIO_NO_ENCONTRADO);
		}

	}

	@Override
	public List<EstadoCuentaResponseDto> getDetalleMovimientos(EstadoCuentaRequestDto estadoCuentaRequestDto)
			throws Exception {
		
		List<EstadoCuentaResponseDto> listaEstadoCuentaResponseDto = new ArrayList<>(); 
		List<Object[]> listaEstadoCuentaNativo = new ArrayList<>();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date dateInicio = format.parse(estadoCuentaRequestDto.getFechaInicio());
		Date dateFin = format.parse(estadoCuentaRequestDto.getFechaFin());
		listaEstadoCuentaNativo = movimientosDao.consultarEstadoCuenta(estadoCuentaRequestDto.getIdentificacion(), dateInicio, dateFin);
		
		listaEstadoCuentaNativo.stream().forEach(
			x -> {
				listaEstadoCuentaResponseDto.add(
					new EstadoCuentaResponseDto(
						x[0] != null ? x[0].toString() : "",
						x[1] != null ? x[1].toString() : "",
						x[2] != null ? x[2].toString() : "",
						x[3] != null ? x[3].toString() : "",
						x[4] != null ? x[4].toString() : "",
						x[5] != null ? x[5].toString() : "",
						x[6] != null ? x[6].toString() : "",
						x[7] != null ? x[7].toString() : ""
					)
				);
			}				
		);
		
		return listaEstadoCuentaResponseDto;
	}

}
