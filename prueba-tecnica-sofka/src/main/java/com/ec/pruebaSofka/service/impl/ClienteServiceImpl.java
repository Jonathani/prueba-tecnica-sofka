package com.ec.pruebaSofka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ec.pruebaSofka.dao.ClienteDao;
import com.ec.pruebaSofka.dao.PersonaDao;
import com.ec.pruebaSofka.dto.ClienteEditRequestDto;
import com.ec.pruebaSofka.dto.ClienteRequestDto;
import com.ec.pruebaSofka.entity.Cliente;
import com.ec.pruebaSofka.entity.Persona;
import com.ec.pruebaSofka.service.ClienteService;
import com.ec.pruebaSofka.utilitarios.UtilitarioGeneral;
import com.ec.pruebaSofka.utilitarios.UtilitarioMensajes;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteDao clienteDao;

	@Autowired
	PersonaDao personaDao;

	@Override
	public void pushCliente(ClienteRequestDto clienteRequestDto) throws Exception {

		if (personaDao.findPersonaById(clienteRequestDto.getIdentificacion()) == null) {
			Persona persona = new Persona();
			persona.setNombre(clienteRequestDto.getNombre().trim());
			persona.setGenero(clienteRequestDto.getGenero().trim());
			persona.setEdad(UtilitarioGeneral.obtenerEdadPersona(clienteRequestDto.getFechaNacimiento()));
			persona.setIdentificacion(clienteRequestDto.getIdentificacion().trim());
			persona.setDireccion(clienteRequestDto.getDireccion().trim());
			persona.setTelefono(clienteRequestDto.getTelefono().trim());
			personaDao.save(persona);

			Cliente cliente = new Cliente();
			cliente.setContrasenia(UtilitarioGeneral.generarClaveAleatoria(UtilitarioMensajes.LONGITUD_CONTRASENIA));
			cliente.setEstado(true);
			cliente.setPersona(persona);
			clienteDao.save(cliente);

		} else {
			throw new Exception(UtilitarioMensajes.USUARIO_REGISTRADO);
		}
	}

	@Override
	public void putCliente(Long idCliente, ClienteEditRequestDto clienteEditRequestDto) throws Exception {

		Cliente cliente = clienteDao.findClienteById(idCliente);
		cliente.setContrasenia(UtilitarioGeneral.generarClaveAleatoria(UtilitarioMensajes.LONGITUD_CONTRASENIA));
		cliente.setEstado(true);
		clienteDao.save(cliente);

		Persona persona = cliente.getPersona();
		persona.setNombre(clienteEditRequestDto.getNombre().trim());
		persona.setGenero(clienteEditRequestDto.getGenero().trim());
		persona.setEdad(UtilitarioGeneral.obtenerEdadPersona(clienteEditRequestDto.getFechaNacimiento()));
		persona.setDireccion(clienteEditRequestDto.getDireccion().trim());
		persona.setTelefono(clienteEditRequestDto.getTelefono().trim());
		personaDao.save(persona);

	}

	@Override
	public void deleteCliente(Long idCliente) throws Exception {

		if (clienteDao.findClienteById(idCliente) != null) {
			Cliente cliente = clienteDao.findClienteById(idCliente);
			clienteDao.delete(cliente);

			Persona persona = cliente.getPersona();
			personaDao.delete(persona);
		} else {
			throw new Exception(UtilitarioMensajes.USUARIO_NO_ENCONTRADO);
		}
	}

	@Override
	public Cliente mostrarInfoCliente(Long idCliente) {
		
		return clienteDao.findClienteById(idCliente);
	}

}
