package com.ec.pruebaSofka.service;

import com.ec.pruebaSofka.dto.ClienteEditRequestDto;
import com.ec.pruebaSofka.dto.ClienteRequestDto;
import com.ec.pruebaSofka.entity.Cliente;

public interface ClienteService {
	
	void pushCliente(ClienteRequestDto clienteRequestDto) throws Exception;
	
	void putCliente(Long idCliente, ClienteEditRequestDto clienteEditRequestDto) throws Exception;
	
	void deleteCliente (Long idCliente) throws Exception;
	
	Cliente mostrarInfoCliente(Long idCliente);

}
