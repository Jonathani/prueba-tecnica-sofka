package com.ec.pruebaSofka.service;

import java.util.List;

import com.ec.pruebaSofka.dto.EstadoCuentaRequestDto;
import com.ec.pruebaSofka.dto.EstadoCuentaResponseDto;
import com.ec.pruebaSofka.dto.MovimientoRequestDto;

public interface MovimientosService {
	
	void pushMovimientos(MovimientoRequestDto movimientoRequestDto) throws Exception;
	
	List<EstadoCuentaResponseDto> getDetalleMovimientos(EstadoCuentaRequestDto estadoCuentaRequestDto) throws Exception;

}
