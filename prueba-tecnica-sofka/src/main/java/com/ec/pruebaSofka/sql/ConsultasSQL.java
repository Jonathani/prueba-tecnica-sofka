package com.ec.pruebaSofka.sql;

public class ConsultasSQL {
	
	public static final String CONSULTA_ESTADO_CUENTA = "select fecha, nombre, numero_cuenta, tipo_cuenta, saldo_inicial, estado, valor, saldo from (\n"
			+ "select mov.fecha, \n"
			+ "MAX (mov.fecha) over (partition by cta.numero_cuenta) as fecha_max,\n"
			+ "per.nombre, cta.numero_cuenta, cta.tipo_cuenta, cta.saldo_inicial, cta.estado, mov.valor, mov.saldo  \n"
			+ "from public.movimientos mov\n"
			+ "inner join public.cuenta cta\n"
			+ "on (mov.cuenta_id  = cta.cuenta_id)\n"
			+ "inner join public.cliente cli\n"
			+ "on (cli.cliente_id = cta.cliente_id)\n"
			+ "inner join public.persona per\n"
			+ "on (cli.persona_id = per.persona_id)\n"
			+ "where per.identificacion = ?1 \n"
			+ "and date(mov.fecha) >= ?2 \n"
			+ "and date(mov.fecha) <= ?3 \n"
			+ ") c where fecha = fecha_max;";

}
