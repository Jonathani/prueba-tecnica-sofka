package com.ec.pruebaSofka;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ec.pruebaSofka.dao.ClienteDao;
import com.ec.pruebaSofka.entity.Cliente;
import com.ec.pruebaSofka.entity.Persona;
import com.ec.pruebaSofka.service.impl.ClienteServiceImpl;
import com.ec.pruebaSofka.utilitarios.UtilitarioGeneral;
import com.ec.pruebaSofka.utilitarios.UtilitarioMensajes;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClienteTestUnit {
	
	@Mock
	ClienteDao clienteDao;
	
	@InjectMocks
	ClienteServiceImpl clienteService;
	
	@Test
    public void unitTestGetClienteById() {
		Persona persona = new Persona();
		persona.setPersonaId(5L);
		persona.setNombre("Jonathan Imbaquingo");
		persona.setGenero("Masculino");
		persona.setEdad(UtilitarioGeneral.obtenerEdadPersona("1989-10-02"));
		persona.setIdentificacion("1718297383");
		persona.setDireccion("Guamani");
		persona.setTelefono("0996799486");

		Cliente cliente = new Cliente();
		cliente.setClienteId(5L);
		cliente.setContrasenia(UtilitarioGeneral.generarClaveAleatoria(UtilitarioMensajes.LONGITUD_CONTRASENIA));
		cliente.setEstado(true);
		cliente.setPersona(persona);
		
		Mockito.when(clienteDao.findClienteById(5L)).thenReturn(cliente);
		
		// Llamar al método a probar
		Cliente result = clienteService.mostrarInfoCliente(5L);

        // Verificar el resultado
        assertNotNull(result);
        assertEquals("Mabel Ortiz", result.getPersona().getNombre());
		
	}
	
	
}
