package com.ec.pruebaSofka;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.ec.pruebaSofka.dao.ClienteDao;
import com.ec.pruebaSofka.dao.PersonaDao;
import com.ec.pruebaSofka.entity.Cliente;
import com.ec.pruebaSofka.entity.Persona;
import com.ec.pruebaSofka.utilitarios.UtilitarioGeneral;
import com.ec.pruebaSofka.utilitarios.UtilitarioMensajes;

@SpringBootTest
@AutoConfigureMockMvc
public class ClienteTestIntegration {
	
	@Autowired
	ClienteDao clienteDao;

	@Autowired
	PersonaDao personaDao;
	
	@Test
	public void integrationTestGetClienteById() {
		Persona persona = new Persona();
		persona.setNombre("Jonathan Imbaquingo");
		persona.setGenero("Masculino");
		persona.setEdad(UtilitarioGeneral.obtenerEdadPersona("1989-10-02"));
		persona.setIdentificacion("1718297383");
		persona.setDireccion("Guamani");
		persona.setTelefono("0996799486");
		personaDao.save(persona);
		

		Cliente cliente = new Cliente();
		cliente.setContrasenia(UtilitarioGeneral.generarClaveAleatoria(UtilitarioMensajes.LONGITUD_CONTRASENIA));
		cliente.setEstado(true);
		cliente.setPersona(persona);
		clienteDao.save(cliente);
		
		Cliente clienteFound = clienteDao.findClienteById(cliente.getClienteId());
		
        assertThat(clienteFound).isNotNull();
        assertThat(clienteFound.getPersona().getNombre()).isEqualTo("Jonathan Imbaquingo");
	}

}
